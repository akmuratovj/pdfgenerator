package uz.pdf.pdfgenerator.service;


import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.borders.DashedBorder;
import com.itextpdf.layout.element.*;
import com.itextpdf.layout.properties.HorizontalAlignment;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.VerticalAlignment;
import org.springframework.stereotype.Service;


import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class PDFGeneratorService {


    public void export(HttpServletResponse response) throws IOException {

        generatePdf();
    }
//

    private static final String FILE_NAME = "/Users/jonibek/Desktop/0/";
//    private static final String FILE_NAME = "D:/";

    private void generatePdf() {
        String talabaIsmi = " Saidov Sirojiddin Jumaqulovich ";
        try {

            PdfFont font = PdfFontFactory.createFont("src/main/resources/fonts/times.ttf", PdfEncodings.IDENTITY_H);


            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
            Date date = simpleDateFormat.parse("12.12.2019");

            //PDF GA YOZISH UCHUN FAYL YARATADI
            PdfWriter writer = new PdfWriter(FILE_NAME + talabaIsmi + (Math.random() * 10000) + ".pdf");

            //PDF GA YOZISH UCHUN PDF DOCUMENT YARATADI
            PdfDocument pdfDocument = new PdfDocument(writer);

            Document document = new Document(pdfDocument);
//            document.setLeftMargin(70f);
//            document.setRightMargin(50f);
//            document.setTopMargin(70f);
//            document.setBottomMargin(70f);

            document.setLeftMargin(50);
            document.setRightMargin(50);
            document.setTopMargin(50f);
            document.setBottomMargin(35f);


            Paragraph paragraph = new Paragraph();
            paragraph.setFont(font);



            Text text = new Text("Шартнома №_________");
            paragraph.setFontSize(10f);
            paragraph.setTextAlignment(TextAlignment.CENTER);
            paragraph.setBold();
            paragraph.add(text);
            document.add(paragraph);

//            textAligmentCenterAndBold("fff", paragraph);
//            textNormal("fffff", paragraph);
//            paragraphAligmentCenterAndBold(font);

            paragraph = new Paragraph();
            text = new Text("“_____”_____________ 2022 \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t Тошкент шаҳри");
            paragraph.setFont(font);
            paragraph.setFontSize(10f);
            paragraph.setTextAlignment(TextAlignment.CENTER);
            paragraph.add(text);
            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);
            paragraph.setFont(font);
            paragraph.setFontSize(10f);
            ;


            text = new Text("“INFLEX INC” МЧЖ (кейинги ўринларда ");

            paragraph.add(text);


            text = new Text("PDP Academy");
            text.setBold();
            paragraph.add(text);

            text = new Text(") номидан раҳбар");
            paragraph.add(text);

            text = new Text(" Матқосимов Ҳикматилло Махмуджон ўғли");
            text.setBold();
            paragraph.add(text);

            text = new Text(" муассаса Низомига асосан бир томондан ва иккинчи томондан ");
            paragraph.add(text);

            text = new Text(talabaIsmi);
            text.setBold();
            paragraph.add(text);

            text = new Text(" (кейинги ўринларда “Талаба” деб номланади) ушбу шартномани туздилар.");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.CENTER);

            text = new Text("1.\tТушунчалар");
            text.setBold();
            paragraph.add(text);

            document.add(paragraph);

            paragraph = new Paragraph();
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            paragraph.setFont(font);
            paragraph.setFontSize(10f);

            text = new Text("Таълим шакли ");
            text.setBold();
            paragraph.add(text);

            text = new Text("– ўқув жараёнларини амалга оширишда қўлланиладиган формат:");
            paragraph.add(text);

            text = new Text("Одатий- ");
            text.setBold();
            paragraph.add(text);

            text = new Text("таълимнинг кенг тарқалган шакли бўлиб, мутахассислик, курс ёки технология бўйича амалга оширилиши мумкин;");
            paragraph.add(text);

            text = new Text("Bootcamp-");
            text.setBold();
            paragraph.add(text);

            text = new Text("таълимнинг интенсив шакли бўлиб, маълум бир мутахассислик бўйича ташкил қилинади; Индивидуал");
            paragraph.add(text);

            document.add(paragraph);

            paragraph = new Paragraph();
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);
            paragraph.setFont(font);
            paragraph.setFontSize(10f);

            text = new Text("Таълим тури –");
            text.setBold();
            paragraph.add(text);

            text = new Text(" таълим жараёнини амалга ошириш усули");

            paragraph.add(text);

            text = new Text("\tOnline\t");
            text.setBold();
            paragraph.add(text);

            text = new Text("(таълим жараёнлари PDP Academyнинг расмий wеб-платформасидан фойдаланган ҳолда амалга оширилади);");
            paragraph.add(text);

            text = new Text("\tOffline\t");
            text.setBold();
            paragraph.add(text);

            text = new Text(" (таълим жараёнлари PDP Academy биносида анъанавий тарзда олиб борилади), ");
            paragraph.add(text);

            text = new Text("\tOnside\t");
            text.setBold();
            paragraph.add(text);

            text = new Text(" (Online va Offline birgalikda); ");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);
            paragraph.setFont(font);
            paragraph.setFontSize(10f);


            text = new Text("Ментор, Устоз ");
            text.setBold();
            paragraph.add(text);

            text = new Text("– таълим тури ва шаклида белгиланган тартибда индивидуал ёки гуруҳ талабаларига дарс машғулотларини олиб борувчи шахс, PDP Academy ходими");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);
            paragraph.setFont(font);
            paragraph.setFontSize(10f);

            text = new Text("Тарафлар (томонлар) – PDP Academy ва талаба. ");
            text.setBold();
            paragraph.add(text);
            document.add(paragraph);

            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.CENTER);


            text = new Text("2.\tТўлов миқдори ва тартиби");
            text.setBold();
            paragraph.add(text);

            document.add(paragraph);

            // Shu yerda table chiziladi

//            float[] pointColumnWidth = {100f,100f};
//            Table table1 = new Table(pointColumnWidth, true);
//            Border border1 = new DashedBorder(ColorConstants.BLACK, 2f);
//            table1.setBorder(border1);
//            table1.addCell(new Cell().add());
//            table1.addCell(new Cell().add("Raju"));
//            table1.addCell(new Cell().add("Id"));
//            table1.addCell(new Cell().add("1001"));
//            table1.addCell(new Cell().add("Designation"));
//            table1.addCell(new Cell().add("Programmer"));

//
//            document.add(new Paragraph("fffff"));

            // Creating an ImageData object
            String imageFile = "files/tablee.png";
            ImageData data = ImageDataFactory.create(imageFile);

            // Creating an Image object
            Image img = new Image(data);

            // Adding image to the document
            document.add(img);

//            document.add(new Paragraph("fffff"));
//


//            Table table1 = new Table(10);
//            table1.addHeaderCell("#");
//            table1.addHeaderCell("Курс номи");
//            table1.addHeaderCell("Таълим тури");
//            table1.addHeaderCell("Таълим \n" +
//                    "шакли\n");
//            table1.addHeaderCell("Дарслар\n" +
//                    "сони\n");
//            table1.addHeaderCell("Дарс \n" +
//                    "давомийлиги\n");
//            table1.addHeaderCell("Нархи");
//            table1.addHeaderCell("ҚҚС");
//            table1.addHeaderCell("Чегирма");
//            table1.addHeaderCell("Жами");
//            table1.getCell(7, 2);
//            table1.addHeaderCell("1");
//            table1.addHeaderCell("Foundation");
//            table1.addHeaderCell("Offline");
//            table1.addHeaderCell("Odatiy");
//            table1.addHeaderCell("24");
//            table1.addHeaderCell("3 соат");
//            table1.addHeaderCell("2 200 000");
////            table1.getColumnWidth(23);
//            table1.addHeaderCell("0% | 0");
//            table1.addHeaderCell("%");
//            table1.addHeaderCell("2 100 000");
//
//            paragraph.add(table1);
//            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);
            paragraph.setFont(font);
            paragraph.setFontSize(10f);


            text = new Text("Жами: 2 100 000 (Икки миллион) сўм");
            text.setBold();
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);
            paragraph.setFont(font);
            paragraph.setFontSize(10f);

            text = new Text("2.1.\tТаълим хизмати  ҳақ эвазига ва тўловни олдиндан тўлиқ миқдорда амалга ошириш шартларида тақдим қилинади.");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);
            paragraph.setFont(font);
            paragraph.setFontSize(10f);


            text = new Text("2.2.\tТалаба Шартнома бўйича тўловларни шартнома имзоланган кундан бошлаб 3 банк куни ичида 100 % миқдоридаги олдиндан тўлов шаклида PDP Academyнинг ҳисоб рақамига амалга оширади.");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);
            paragraph.setFont(font);
            paragraph.setFontSize(10f);
            ;

            text = new Text("2.3.\tХизмат кўрсатиш талаба тўлов қилган курс бўйича гуруҳ шакллангандан сўнг бошланади.");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);
            paragraph.setFont(font);
            paragraph.setFontSize(10f);
            ;

            text = new Text("2.4.\tТаълим хизмати бўйича иш бошлангандан сўнг амалда хизматнинг кўрсатилганлиги ва ҳажмидан қатъи назар Талаба томонидан амалга оширилган тўлов маблағлари қайтарилмайди.");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);
            paragraph.setFont(font);
            paragraph.setFontSize(10f);

            text = new Text("2.5.\tТалаба PDP Academyдан талабалар сафидан четлаштирилганда тўловлар қайтарилмайди. ");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);
            paragraph.setFont(font);
            paragraph.setFontSize(10f);
            ;

            text = new Text("2.6.\tPDP Academy томонидан берилган чегирма асосида ўқишни бошлаган талаба шартномани бекор қилганда, чегирма бекор бўлади ва кўрсатилган хизматлар чегирмасиз ҳисоб китоб қилинади");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);
            paragraph.setFont(font);
            paragraph.setFontSize(10f);
            ;

            text = new Text("2.7.\tШартноманинг 3.9. бандидаги ҳолат бўйича тўлов қайтарилганда ёзма  равишда ариза берилган санадан бошлаб ");
            paragraph.add(text);

            text = new Text("14 (ўн тўрт) банк иш куни ");
            text.setBold();
            paragraph.add(text);

            text = new Text("нақд пул кўринишида қайтарилади");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.CENTER);

            text = new Text("3.\tТаълим жарёни");
            text.setBold();
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);
            paragraph.setFont(font);
            paragraph.setFontSize(10f);
            ;

            text = new Text("3.1.\tТаълим хизмати ўзбек тилида, ҳақ эвазига, тўловни олдиндан тўлиқ миқдорда амалга ошириш шартларида тақдим қилинади. Шартномада кўрсатилган маблағни тўлиқ тўламаган тақдирда Талаба дарс жараёнларига қўйилмайди");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);
            paragraph.setFont(font);
            paragraph.setFontSize(10f);
            ;

            text = new Text("3.2.\tЎқиш давомида курс материалларини ўзлаштириш ҳолатини баҳолаш мақсадида талабалар ўқув режасига кўра оралиқ шаклда, шунингдек ўқув курси якунига кўра якуний имтиҳон ўтказилади.");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);
            paragraph.setFont(font);
            paragraph.setFontSize(10f);
            ;

            text = new Text("3.3.\tТалаба PDP Academyдан қуйидаги ҳолларда четлаштирилиши мумкин:\n" +
                    "ўз хоҳишига биноан, оралиқ ва якуний имтиҳонлардан қониқарсиз натижа кўрсатса, ўқув интизомини ва" +
                    " PDP Academyнинг ички тартиб-қоидалари ҳамда одоб-ахлоқ қоидаларини бузса, " +
                    "30 кун кесимида узрли сабабларсиз 8 соатдан ортиқ дарс қолдирса, ўқиш учун белгиланган тўлов ўз вақтида амалга оширилмаса, " +
                    "суд томонидан озодликдан маҳрум этилганлиги муносабати билан, саломатлиги туфайли (тиббий комиссияси маълумотномаси асосида.\n");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("3.4.\tТаълим жараёни мобайнида ҳарбий хизматни ўташ, саломатлигини тиклаш, ҳомиладорлик ва туғиш, шунингдек," +
                    " болаларни парвариш қилиш таътиллари даврида ҳамда оиласининг бетоб аъзосини (отаси, онаси ёки " +
                    "уларнинг ўрнини босувчи шахслар, турмуш ўртоғи, фарзанди) парвариш қилиш учун талабага таътил берилмайди.");

            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("3.5.\tТалабани айбли ҳаракати (жумладан, ўқув интизомини ва PDP Academyнинг ички тартиб қоидаларини бузганлиги, узрли сабабларсиз дарс қолдирганлиги ёки ўқитиш учун белгиланган миқдордаги тўловни ўз вақтида амалга оширмаганлиги) сабабли талабалар сафидан четлаштириш PDP Academyнинг мутлоқ ҳуқуқи ҳисобланиб, бунда мазкур ҳаракатларни PDP Academy маъмурияти бошқа органлар билан келишишга мажбур эмас.");

            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("3.6.\tPDP Academy таълим хизматини жараёнини мустақил равишда амалга ошириш, қўшимча касб-ҳунар таълими дастури доирасида ўқишни ташкил этиш ва унинг тартибини белгилаш, таълим дастури, унинг давомийлиги ва машғулотлар жадвалини тузиш ёки ўзгартириш, машғулотлар бошланишидан олдин ва машғулотлар давомида Талабалар сафини белгилаш ва ўзгартириш, шунингдек Талабанинг жорий, оралиқ ва якуний имтиҳондан ўтказиш тартибини белгилаш ва ўзгартиришга ҳақли");

            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("3.7.\tТалаба ўқиш жараёнида ўзига юкланган вазифаларни PDP Academy томонидан тайинланган Менторнинг кузатуви ва мувофиқлаштируви остида мақбул ва самарали бажариши лозим.");

            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("3.8.\tТалаба томонидан ҳар қандай сабабга кўра қолдирилган машғулотлар учун ҳисобланган тўлов миқдорини PDP Academy қайтариш ёки ҳар қандай шаклда қоплаш мажбуриятини олмайди. Талаба дарс қолдирган тақдирда, PDP Academy қолдирилган дарсни қўшимча вақтда ўтиб бериш мажбуриятини олмайди. ");

            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("3.9.\tКурс бошланишида талаба дастлабки 2 та дарс давомида курс бўйича ўз фикрини билдириб, 3-дарс бошлангунга қадар ёзма равишда ариза бериб ўқишини тўхтатиши ва  тўловини қайтариб олиши мумкин. Курснинг 3 дарси бошлангандан  сўнг тўлов қайтарилмайди.");
            text.setHorizontalScaling(1f);
            text.setTextRise(1f);
            text.setDestination("dddddd");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("3.10.\tТалабага якуний имтиҳондан ўтиш учун имкониятлар муваффақиятсиз бўлса, таълим хизматлари кўрсатилган ҳисобланади ва шартнома бажарилади.");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("3.11.\tТалаба таълим дастурини ўзлаштириб, якуний имтиҳондан муваффақиятли ўтганидан сўнг унга мутахассис тайёрлаш дастурининг таълим хизматларидан ўтганлиги тўғрисида Сертификат берилади.");

            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("3.12.\tАгар талаба якуний имтиҳонда иштирок этмаса ёки қониқарсиз натижа кўрсатса ёки ўқув дастурининг бир қисмини ўзлаштирган бўлса, ўқув дастурини 100% тамомламаган бўлса ўқув дастурига мувофиқ ўқитилганлиги тўғрисида Сертификат берилмайди");

            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);


            text = new Text("3.13.\tАгар талаба модуллар якунида якуний имтиҳон натижасидан қониқарсиз натижа кўрсатса, қайта топшириш имтиҳонини PDP academy белигалаган тўлов орқали топшириши мумкин.");

            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.CENTER);

            text = new Text("4.\tТарафларнинг ҳуқуқлари ва мажбуриятлари");
            text.setBold();
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.1.\tТалаба қуйидаги ҳуқуқларга эга:");
            text.setBold();
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.1.1.\tмазкур шартноманинг ижроси юзасидан, шунингдек хизматдан фойдаланиш жараёнида вужудга келган саволлар ва/ёки уларга берилган жавобларда ноаниқлик ва хато аниқлаган тақдирда PDP Academyдан қўшимча маълумотлар олишга ҳақли;");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.1.2.\tтаълим жараёни самарадорлиги ва таълим сифатини ошириш юзасидан ўз таклиф-мулоҳазаларини, танқидий фикрларини белгиланган тартибда таълим муассасаси маъмуриятига билдириш ва уларни кўриб чиқилишини талаб қилиш;");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.1.3.\tPDP Academy тўғрисида маълумот олиш ва ушбу шартномада назарда тутилган таълим хизматлари тўғри кўрсатилишини талаб этиш;");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.1.4.\tТалабаларнинг билими, кўникмалари ва малакасини баҳолаш мезонлари ҳақида маълумот ва маслаҳат олиш;");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.1.5.\tинсон қадр-қимматини ҳурмат қилиш, жисмоний ва руҳий зўравонликнинг барча шаклларидан ҳимоя қилиш, ҳақорат, ҳаёт ва саломатликни муҳофаза қилиш юзасидан зарур чоралар кўришни талаб қилиш;");
            paragraph.add(text);

            document.add(paragraph);

            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.1.6.\tТартибга солувчи қонунда белгиланган ҳуқуқлар.");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.2.\tТалаба қуйидаги мажбуриятларга эга:");
            text.setBold();
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.2.1.\tтаълим дастурлари ва ўқув режасида назарда тутилган билимларни тўлиқ ўзлаштириши, топшириқларнинг барча турларини белгиланган муддатда бажариши;");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.2.2.\tўз ҳуқуқларини суистеъмол қилмаслиги, бу ҳуқуқлардан ўзга шахслар манфаатига зид мақсадларда фойдаланмаслиги;");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.2.3.\tумумий қабул қилинган хулқ-атвор нормаларига, шунингдек PDP Academyнинг Уставига, Ички тартиб қоидалари, санитария ва ҳавфсизлик талабларига ҳамда бошқа локал ҳужжатларига риоя этиш;");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.2.4.\tPDP Academyга ўзининг шахсига оид ҳужжатлар нусхалари тақдим этиши;");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.2.5.\tмашғулотларга белгиланган вақтда ва тўлиқ иштирок этиши, берилган топшириқ ва вазифаларни тўғри ва тўлиқ бажариши;");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.2.6.\tмашғулотларда курс талабига мос келадиган компьютер (ноутбук) билан иштирок этиш");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.3.\tPDP Academy қуйидаги ҳуқуқларга эга:");
            text.setBold();
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.3.1.\tТалаба учун мажбурий бўлган буйруқ ва кўрсатмалар бериш;");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.3.2.\tўз хоҳишига кўра таълим дастури, машғулот жадваллари, материаллари," +
                    " Платформа ва Aхборот ресурсининг таркибини шакллантириш, унинг алоҳида қисмларини ўзгартириш," +
                    " янгилаш, улардан фойдаланиш тартибини белгилаш, шунингдек Платформа ва " +
                    "Aхборот ресурсига асосланган ҳолда мустақил ёки унга боғланган бошқа турдаги хизматларни таклиф этиш");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.3.3.\tТалаба томонидан мазкур шартномада ва локал ҳужжатларда белгиланган мажбуриятларнинг бажарилмаслиги ёки лозим даражада бажарилмаслиги PDP Academy томонидан қонунда ва ушбу қоидаларда белгиланган тартибда чора кўриш;");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.3.4.\tPDP Academy томонидан ўрнатилган ички тартиб қоидаларга риоя қилмаган Талабани тартибга чақириш, талабалар сафидан чиқариш ва шартномани бекор қилиш;");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.3.5.\tТалабанинг аккаунти (шахсий кабинети) орқали бажарилган барча харакатлар талаба томонидан бажарилган деб ҳисоблаш;");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.3.6.\tТалаба томонидан Платформада амалга оширган ҳар қандай ҳаракат учун жавобгар бўлмайди;");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.3.7.\tТалаба томонидан ушбу қоидаларда белгиланган мажбуриятларни қўпол равишда бузилган ёки фаолиятни ташкил этиш жараёнида бошқа қонунбузарликлар аниқланган тақдирда, тегишли ҳуқуқни муҳофаза қилувчи органларга мурожаат этиш;");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.3.8.\tПлатформа интерфейси ва хизматлар кўрсатиш жараёнига, шунингдек, мазкур фойдаланиш қоидаларига бир томонлама ўзгартириш ва қўшимчалар киритиш;");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.3.9.\tТалаба томонидан тўлов тўлиқ амалга оширилмагунига қадар хизмат кўрсатмаслик;");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.3.10.\tушбу шартнома, PDP Academy локал ҳужжатлари ва Тартибга солувчи қонунда белгиланган бошқа ҳуқуқлар.");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.4.\tPDP Academy қуйидаги мажбуриятларга эга:");
            text.setBold();
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.4.1.\tТалабани машғулот ўтказиш учун керакли жиҳозларга эга машғулот хоналари, шунингдек зарур ҳолларда ўқув қўлланмалари билан электрон ёки босма шаклда билан таъминлаш;");

            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.4.2.\tталабани PDP Academyнинг Уставига, Ички тартиб қоидалари, санитария ва ҳавфсизлик талабларига ҳамда бошқа локал ҳужжатлари талаблари билан таништириш;");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.4.3.\tшартномада назарда тутилган таълим хизматларини ўз вақтида, тўғри кўрсатишни таъминлаш;");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.4.4.\tталабага танланган ўқув дастурида назарда тутилган ривожланиш учун шароит яратиб бериш;");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.4.5.\tўқув курсини лозим даражада ўзлаштирган малакали мутахассислар иштирокида таълим хизматларини кўрсатиш;");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.4.6.\tУшбу шартнома ва PDP Academyнинг локал ҳужжатларида назарда тутилган мажбуриятларни бузган тақдирда талабага нисбатан қуйидаги интизомий жазо чоралари кўрилади:\n" +
                    "-\tогоҳлантириш;\n" +
                    "-\tталабалар сафидан четлатиш.\n");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("4.4.7.\tТалаба Тартибга солувчи қонун ва PDP Academyнинг локал ҳужжатлари талаблари ҳамда PDP Academy томонидан тайинланган Устознинг топшириқларини бажармаслик, ўзбошимчалик билан ҳаракатланиш, қасддан зарар етказиш оқибатида юзага келадиган ҳар қандай ҳолат юзасидан шахсан жавобгар бўлади.");

            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.CENTER);

            text = new Text("5.\tИнтеллектуaл мулк");
            text.setBold();
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("5.1.\tТалабага таълим жараёнида бевосита тақдим этилган, шунингдек Платформада жойлаштирилган ");
            text.setBold();
            paragraph.add(text);

            text = new Text("Aхборот ресурсини ташкил этувчи ва барча объектлар, шу жумладан чекланмаган маълумотлар, матнлар, материаллар, график элементлар, дизайн, расмлар, фото ва видео материаллар, мусиқа, скриптлар, компютер дастурлари, хизматлар, тузилма ва уларга бўлган ҳуқуқлар («Интеллектуaл мулк объектлари») PDP Academyга тегишли.");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("5.2.\tТалаба унга фойдаланиш учун тақдим қилинган барча турдаги Интеллектуaл мулк объектларини учинчи шахсларга ҳар қандай шаклда тарқатиши тақиқланади.");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("5.3.\tТалаба Интеллектуaл мулк объектларидан фақат шахсий мақсадларида мазкур шартнома, PDP Academy локал ҳужжатлари ва қонунда назарда тутилган шартлар доирасида Платформанинг функционал имкониятлари доирасида фойдаланишга ҳақли.");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("5.4.\tТалаба Платформа орқали фойдаланган Интеллектуaл мулк объектларининг уникал ID рақами воситасида Интеллектуaл мулк объектларининг учинчи шаҳсларга Талаба орқали тарқатилганлиги аниқланса талабани жавобгарликка тортишга асос бўлади.");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("5.5.\tИнтеллектуaл мулк объектларидан рухсатсиз фойдаланишнинг олдини олиш мақсадида PDP Academy ҳар қандай ҳимоя воситаларидан – Интеллектуaл мулк объектларига киришни назорат қилувчи, тақиқловчи, чекловчи технологиялар, техник воситалар ёки уларнинг таркибий қисмларидан фойдаланишга ҳақли");
            paragraph.add(text);

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.CENTER);

            text = new Text("6.\tТарафларнинг жавобгарлиги");
            paragraph.add(text);
            text.setBold();

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("6.1.\tТарафлар мазкур шартнома ва қонун талабларини бузганлик, мажбуриятларини бажармаганлик ёки лозим даражада бажармаганлик учун қонунда белгиланган тартибда жавобгар бўладилар.");
            paragraph.add(text);


            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.CENTER);

            text = new Text("7.\tКоррупцияга қарши шартлар");
            paragraph.add(text);
            text.setBold();

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("7.1.\tШартнома Томонлардан бири ҳар қандай коррупцияга қарши шартларни бузилиши содир бўлган ёки содир бўлиши мумкин деб гумон қилса, тегишли Томон бошқа Томонни ёзма ёки оғзаки равишда, шу жумладан, ишонч телефони орқали хабардор қилиш мажбуриятини олади.");
            paragraph.add(text);


            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.CENTER);

            text = new Text("8.\tФорс-мажор");
            paragraph.add(text);
            text.setBold();

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("8.1.\tТомонлар ушбу Шартнома бўйича мажбуриятларни бажармаганлиги ёки лозим даражада бажармаганлиги учун, агар бундай вазият ушбу Шартнома тузилгандан кейин Томонлар олдиндан билиши ва унинг олдини олиши мумкин бўлмаган фавқулодда хусусиятга эга воқеалар натижасида вужудга келган енгиб бўлмайдиган куч (форс-мажор ҳолатлари) туфайли юзага келганлигини ва Томонлар ўз мажбуриятларини лозим даражада бажариш юзасидан барча мумкин бўлган ва ўзларига боғлиқ бўлган чораларни кўрганлигини исботласа, жавоб бермайди. ");
            paragraph.add(text);


            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.CENTER);

            text = new Text("9.\tШартномани муддати, уни ўзгартириш ва бекор қилиш тартиби");
            paragraph.add(text);
            text.setBold();

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("9.1.\tУшбу Шартнома имзоланган кундан бошлаб кучга киради ва ушбу томонлар Шартнома бўйича ўз мажбуриятларини тўлиқ бажаргунига қадар амал қилади.");
            paragraph.add(text);


            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("9.2.\tТарафлар мазкур шартномада, PDP Academy локал ҳужжатларида ва Ўзбекистон Республикаси қонун ҳужжатларида белгиланган тартибда ва асосларда ушбу Шартномани муддатидан олдин бекор қилиш ҳуқуқига эга.");
            paragraph.add(text);


            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.CENTER);

            text = new Text("10.\tТартибга солувчи қонун ва Низоларни ҳал қилиш");
            paragraph.add(text);
            text.setBold();

            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("10.1.\tМазкур шартнома, шу жумладан унинг ҳақиқийлиги, ижроси ва бекор қилиниши билан боғлиқ муносабатлар Ўзбекистон Республикасининг қонунларига мувофиқ тартибга солинади.");
            paragraph.add(text);


            document.add(paragraph);


            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("10.2.\tТомонлар ўртасида музокаралар ўтказиш йўли билан ҳал этилмаган низолар Ўзбекистон Республикаси қонунчилигида белгиланган тартибда PDP Academy жойлашган жой бўйича суд томонидан кўриб чиқилади. ");
            paragraph.add(text);

            document.add(paragraph);

            paragraph = new Paragraph();
//            paragraph.setMarginBottom(40f);
            document.add(paragraph);

            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.CENTER);

            text = new Text("11.\tЯкуний қоидалар");
            paragraph.add(text);
            text.setBold();

            document.add(paragraph);

            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
            paragraph.setFirstLineIndent(25f);

            text = new Text("11.1.\tТарафларнинг мазкур шартнома билан тартибга солинмаган муносабатлари Ўзбекистон Республикаси қонунлари билан тартибга солинади.");
            paragraph.add(text);


            document.add(paragraph);

            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.CENTER);

            text = new Text("Томонларнинг реквизитлари\n");
            paragraph.add(text);
            text.setBold();

            document.add(paragraph);


            float[] pointColumnWidths = {100f, 20f, 100f};
            Table table = new Table(pointColumnWidths, true);
            Border border = new DashedBorder(ColorConstants.WHITE, 0.5f);
            table.setBorder(border);

            Paragraph paragraphlar = new Paragraph();

            paragraphlar.add(new Text("PDP Academy\n\n").setFixedPosition(65f, 30f, 33f).setFontSize(9f).setBold());
            paragraphlar.add("\n“INFLEX INC” МЧЖ\n" +
                    "Тошкент шаҳри  Шайхонтохур тумани Беруний кўчаси 3А-уй\n" +
                    "Тел:   998 78-777-47-47\n");
            paragraphlar.add(new Text("ҳ/р: 2020 8000 8007 9362 1001\n").setBold());
            paragraphlar.add("АТБ “Универсалбанк”нинг Тошкент филиали\n" +
                    "МФО:00996  \n" +
                    "ИНН:305004536 ОКЭД: 62030\n\n\n");
            paragraphlar.add(new Text(" Директор                                  Матқосимов Ҳ.М.").setUnderline());
            paragraphlar.add("(лавозими)                                    (Ф.И.Ш.) \n");

            paragraphlar.setFontSize(9f);
            paragraph.setMarginTop(1f);
            paragraph.setFixedLeading(1f);

            Cell cell = new Cell();
            cell.setBorder(border);

            cell.setFont(font);
            cell.setHorizontalAlignment(HorizontalAlignment.CENTER);
            cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
            cell.add(paragraphlar);
            cell.setHeight(200f);
            table.addCell(cell);

            cell = new Cell();
            cell.setBorder(border);
            cell.setHeight(200f);
            table.addCell(cell);

            paragraph = new Paragraph();
            paragraph.setFontSize(10f);
            paragraph.setFont(font);
            paragraph.setTextAlignment(TextAlignment.CENTER);

            Paragraph paragraph12 = new Paragraph();

            paragraph12.add(new Text("“Талаба”\n").setFixedPosition(90f, 30f, 33f).setFontSize(9f).setBold());


            paragraph12.add(
                    "\n______________________________________________\n" +
                            "______________________________________________\n" +
                            "Паспорт:______________________________________\n" +
                            "Манзил:_______________________________________\n" +
                            "______________________________________________\n" +
                            "Телефон:______________________________________\n\n" +
                            "Имзо:_________________________________________\n\n\n\n\n");

            paragraph.setMarginTop(1f);
            paragraph.setFixedLeading(1f);
            paragraph12.setFontSize(9f);

            cell.setFontSize(8);
            cell = new Cell();
            cell.setBorder(border);
            cell.setFont(font);
            cell.setHeight(200f);
            cell.setHorizontalAlignment(HorizontalAlignment.CENTER);
            cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
            cell.add(paragraph12);
            table.addCell(cell);


            document.add(paragraph);
            document.add(table);


            document.close();
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

//    private void paragraphAligmentCenterAndBold(PdfFont font) {
//        Paragraph paragraph = new Paragraph();
////        text = new Text("“_____”_____________ 2022 \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t Тошкент шаҳри");
//        paragraph.setFont(font);
//        paragraph.setFontSize(10f);
//        paragraph.setTextAlignment(TextAlignment.CENTER);
////        paragraph.add(text);
//        
//        
////        document.add(paragraph);
//    }
//
//    private void textNormal(String docText, Paragraph paragraph) {
//        Text text = new Text(docText);
//        paragraph.setFontSize(10f);
//        paragraph.add(text);
//    }
//
//    private void textAligmentCenterAndBold(String docText, Paragraph paragraph) {
//
//        Text text = new Text(docText);
//        paragraph.setFontSize(10f);
//        paragraph.setTextAlignment(TextAlignment.CENTER);
//        paragraph.setBold();
//        paragraph.add(text);
////        document.add(paragraph);
//    }

}