//package uz.pdf.pdfgenerator.service;
//
//import com.itextpdf.io.font.PdfEncodings;
//import com.itextpdf.kernel.colors.ColorConstants;
//import com.itextpdf.kernel.font.PdfFont;
//import com.itextpdf.kernel.font.PdfFontFactory;
//import com.itextpdf.kernel.pdf.PdfDocument;
//import com.itextpdf.kernel.pdf.PdfWriter;
//import com.itextpdf.layout.Document;
//import com.itextpdf.layout.borders.Border;
//import com.itextpdf.layout.borders.DashedBorder;
//import com.itextpdf.layout.borders.SolidBorder;
//import com.itextpdf.layout.element.Cell;
//import com.itextpdf.layout.element.Paragraph;
//import com.itextpdf.layout.element.Table;
//import com.itextpdf.layout.element.Text;
//import com.itextpdf.layout.property.TextAlignment;
//
//import java.io.FileNotFoundException;
//import java.io.IOException;
//import java.text.DateFormat;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.Scanner;
//
//public class Application {
//    public static void main(String[] args) {
////        Scanner scanner=new Scanner(System.in);
////
////        System.out.println("Viloyatni kiriting: ");
////        String region=scanner.nextLine();
////
////        System.out.println("Ismingizni kiriting: ");
////        String name=scanner.nextLine();
////
////        System.out.println("Familyangizni kiriting: ");
////        String lastName=scanner.nextLine();
////
////        System.out.println("Otangizni ismini kiriting: ");
////        String middleName=scanner.nextLine();
////
////        System.out.println("Inn raqamni kiriting: ");
////        String inn=scanner.nextLine();
////
////        System.out.println("Tumanni kiriting: ");
////        String district=scanner.nextLine();
////
////        try {
////            Region region1 = new Region(26,region);
////            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.mm.yyyy");
////            Date date = simpleDateFormat.parse("12.12.2019");
////            NalogaPlatelshik nalogaPlatelshik = new NalogaPlatelshik(
////                    lastName, name, middleName,
////                    inn, date, region1, new District(2608,  district,region1));
////
////            //PDF GA YOZISH UCHUN FAYL YARATADI
////            PdfWriter writer = new PdfWriter("D://" + nalogaPlatelshik.getInnRaqami() + ".pdf");
////            //PDF GA YOZISH UCHUN PDF DOCUMENT YARATADI
////            PdfDocument pdfDocument = new PdfDocument(writer);
////
////            Document document = new Document(pdfDocument);
////
////            Paragraph paragraphTop = new Paragraph();
////            paragraphTop.setMarginLeft(300);
////            paragraphTop.setTextAlignment(TextAlignment.CENTER);
////            Text textTop = new Text("Солиқ солиш объектлари ҳамда солиқ" +
////                    "солиш билан боғлик объектлар тўғрисидаги" +
////                    "маълумотлар базасини шакллантириш ва" +
////                    "юритиш тартиби тўғрисидаги" +
////                    "низомга 2-илова");
////            PdfFont font = PdfFontFactory.createFont("src/main/resources/fonts/times.ttf", PdfEncodings.IDENTITY_H, true);
////            textTop.setFont(font);
////            textTop.setFontSize(10f);
////            paragraphTop.setFixedLeading(10f);
////            paragraphTop.add(textTop);
////            Text text = new Text("ЎЗБЕКИСТОН РЕСПУБЛИКАСИ ДАВЛАТ СОЛИҚ ҚЎМИТАСИ\n" +
////                    "ГОСУДАРСТВЕННЫЙ НАЛОГОВЫЙ КОМИТЕТ РЕСПУБЛИКИ УЗБЕКИСТАН\n" +
////                    "Солиқ тўловчининг Ўзбекистон Республикаси Давлат солиқ қўмитасида рўйхатга\n" +
////                    "олинганлиги ва унга идентификация рақами берилганлиги тўғрисида\n" +
////                    "ГУВОҲНОМА\n" +
////                    "УДОСТОВЕРЕНИЕ\n" +
////                    "о регистрации в Государственном налоговом комитете Республики Узбекистан\n" +
////                    "и присвоении идентификационного номера налогоплательщику\n" +
////                    "СОЛИҚ ТЎЛОВЧИ - ЖИСМОНИЙ ШАХС\n" +
////                    "(НАЛОГОПЛАТЕЛЬЩИК - ФИЗИЧЕСКОЕ ЛИЦО):");
////            text.setFont(font);
////
////            Paragraph paragraph2 = new Paragraph();
////            paragraph2.setTextAlignment(TextAlignment.CENTER);
////            paragraph2.add(text);
////            Text text8 = new Text("Фамилияси:\n(Фамилия)");
////            text8.setFont(font);
////            text8.setFontSize(10f);
////            Paragraph paragraph8 = new Paragraph();
////            paragraph8.setMarginBottom(0);
////            paragraph8.setPaddingBottom(0);
////            paragraph8.add(text8);
////
////            Text text9 = new Text(nalogaPlatelshik.getLastName().toUpperCase());
////            text9.setFont(font);
////            text9.setFontSize(10f);
////            Paragraph paragraph9 = new Paragraph();
////            paragraph9.setMarginTop(0);
////            paragraph9.setPaddingTop(0);
////            paragraph9.add(text9);
////
////            Text text10 = new Text("Исми:\n(Имя)");
////            text10.setFont(font);
////            text10.setFontSize(10f);
////            Paragraph paragraph10 = new Paragraph();
////            paragraph10.setMarginBottom(0);
////            paragraph10.setPaddingBottom(0);
////            paragraph10.add(text10);
////
////            Text text11 = new Text(nalogaPlatelshik.getFirstName().toUpperCase());
////            text11.setFont(font);
////            text11.setFontSize(10f);
////
////            Paragraph paragraph11 = new Paragraph();
////            paragraph11.setMarginTop(0);
////            paragraph11.setPaddingTop(0);
////            paragraph11.add(text11);
////
////            Text text12 = new Text("Отасини исми:\n(Отчества)");
////            text12.setFont(font);
////            text12.setFontSize(10f);
////
////            Paragraph paragraph12 = new Paragraph();
////            paragraph12.setMarginBottom(0);
////            paragraph12.setPaddingBottom(0);
////            paragraph12.add(text12);
////
////            Text text13 = new Text(nalogaPlatelshik.getMiddleName().toUpperCase());
////            text13.setFont(font);
////            text13.setFontSize(10f);
////
////            Paragraph paragraph13 = new Paragraph();
////            paragraph13.setMarginTop(0);
////            paragraph13.setPaddingTop(0);
////            paragraph13.add(text13);
////
////                float[] pointColumnWidths = {300f, 300f};
////            Table table = new Table(pointColumnWidths);
////            Border border = new DashedBorder(ColorConstants.BLACK, 2f);
////            //LastName
////            Cell cell1 = new Cell();
////            cell1.setBorder(border);
////            cell1.setFont(font);
////            cell1.add(paragraph8);
////            table.addCell(cell1);
////            //LastName
////            Cell cell2 = new Cell();
////            cell2.setBorder(border);
////            cell2.setFont(font);
////            cell2.add(paragraph9);
////            table.addCell(cell2);
////            //FirstName
////            Cell cell3 = new Cell();
////            cell3.setBorder(border);
////            cell3.add(paragraph10);
////            table.addCell(cell3);
////            //FirstName
////            Cell cell4 = new Cell();
////            cell4.setBorder(border);
////            cell4.add(paragraph11);
////            table.addCell(cell4);
////            //MiddleName
////            Cell cell5 = new Cell();
////            cell5.setBorder(border);
////            cell5.add(paragraph12);
////            table.addCell(cell5);
////            //MiddleName
////            Cell cell6 = new Cell();
////            cell6.setBorder(border);
////            cell6.add(paragraph13);
////            table.addCell(cell6);
////            Text text14 = new Text("Ўзбекистон Республикаси Давлат солиқ қўмитасида рўйхатга олинди ва унга қуйидаги\n" +
////                    "солиқ тўловчи идентификация раками (СТИР) берилди:\n" +
////                    "(Зарегистрирован в Государственном налоговом комитете Республики Узбекистан\n" +
////                    "и ему присвоен идентификационный номер налогоплательщика (ИНН))");
////            text14.setFont(font);
////            text14.setFontSize(10f);
////            Paragraph paragraph14 = new Paragraph();
////            paragraph14.setTextAlignment(TextAlignment.CENTER);
////            paragraph14.add(text14);
////
////            float[] floats = {300f, 300f, 300f, 300f};
////            Table table1 = new Table(floats);
////            Paragraph paragraph16 = new Paragraph();
////            paragraph16.add(nalogaPlatelshik.getInnRaqami());
////            paragraph16.setMarginTop(6f);
////            paragraph16.setBold();
////            paragraph16.setFixedLeading(9f);
////            Cell cell16 = new Cell();
////            cell16.setBorderRight(new SolidBorder(ColorConstants.BLACK, 1f));
////            cell16.setTextAlignment(TextAlignment.CENTER);
////            cell16.add(paragraph16);
////            table1.addCell(cell16);
////
////            Cell cellPustoy = new Cell();
////            cellPustoy.setBorder(border);
////            cellPustoy.setBorderLeft(new SolidBorder(ColorConstants.BLACK, 1f));
////            table1.addCell(cellPustoy);
////
////            Cell cell17 = new Cell();
////            Paragraph paragraph17 = new Paragraph();
////            paragraph17.setFixedLeading(9f);
////            Text text17 = new Text("СТИР берилган сана:\n" +
////                    "(Дата присвоения ИНН)");
////            text17.setFont(font);
////            text17.setFontSize(10f);
////            paragraph17.add(text17);
////            cell17.add(paragraph17);
////            cell17.setBorder(border);
////            table1.addCell(cell17);
////
////            Cell cell18 = new Cell();
////            Paragraph paragraph18 = new Paragraph();
////            paragraph18.setFixedLeading(9f);
////            paragraph18.add(simpleDateFormat.format(nalogaPlatelshik.getRegisterDate()));
////            cell18.add(paragraph18);
////            cell18.setBorder(border);
////            table1.addCell(cell18);
////
////            float[] floats1={150,200,200};
////            Table table2 = new Table(floats1).setFont(font).setMarginTop(10f);
////            Cell cell19 = new Cell();
////            cell19.add(new Paragraph("Рўйхатга олинган ДСИ:\n" +
////                    "(Зарегистирован в ГНИ)").setFixedLeading(10f).setFontSize(10f)).setBorder(border);
////            table2.addCell(cell19);
////             cell19 = new Cell();
////            cell19.setBorder(border);
////            table2.addCell(cell19);
////            cell19 = new Cell();
////            cell19.add(new Paragraph(nalogaPlatelshik.getDistrict().getName().toUpperCase()+"  TUMANI").setFixedLeading(10f).setFontSize(10f))
////                  .setBorder(border);
////            table2.addCell(cell19);
////
////            Paragraph paragraph=new Paragraph("Ушбу Гувоҳнома Давлат солиқ қўмитасининг интернетдаги расмий сайти\n" +
////                    "орқали шакллантирилган ва чоп этилган\n" +
////                    "Данное Удостверение сформировано и распечатано через официальный сайт\n" +
////                    "Государственного налогового комитета").setFont(font).setTextAlignment(TextAlignment.CENTER);
////
////            Table table3=new Table(2).setFont(font).setFontSize(10f);
////            Cell cell9=new Cell();
////            cell9.add(new Paragraph("ДИҚҚАТ!\n" +
////                    "Гувоҳноманинг ҳақиқийлигини текшириб кўриш учун\n" +
////                    "ДСҚнинг 'soliq.uz' сайтидаги 'СТИРингизни аниқланг'\n" +
////                    "интерактив хизмати оркали фукаронинг паспорт\n" +
////                    "маълумотларини киритиб текшириш мумкин")).setBorder(border).setTextAlignment(TextAlignment.CENTER);
////            table3.addCell(cell9);
////            cell9=new Cell();
////            cell9.add(new Paragraph("ВНИМАНИЕ!\n" +
////                    "Проверить достоверность данного удостоверения\n" +
////                    "можно на сайте ГНК 'soliq.uz' посредством\n" +
////                    "интерактивной услуги 'Узнай свой ИНН' путем ввода\n" +
////                    "паспортных данных заявителя")).setBorder(border).setTextAlignment(TextAlignment.CENTER);
////            table3.addCell(cell9);
////            float[] floats2={400f,400f};
////
////            Table table4=new Table(floats2).setFont(font).setFontSize(10f);
////            cell9=new Cell();
////            cell9.add(new Paragraph("Гувоҳнома чоп этилган сана:\n" +
////                    "(Дата распечатки удостоверение)").setFixedLeading(10f)).setBorder(border);
////            table4.addCell(cell9);
////            cell9=new Cell();
////            date=new Date();
////            simpleDateFormat=new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
////            cell9.add(new Paragraph(simpleDateFormat.format(date))).setBorder(border).setTextAlignment(TextAlignment.CENTER);
////            table4.addCell(cell9);
////
////            document.add(paragraphTop);
////            document.add(paragraph2);
////            document.add(table);
////            document.add(paragraph14);
////            document.add(table1);
////            document.add(table2);
////            document.add(paragraph);
////            document.add(table3);
////            document.add(table4);
////            document.close();
////        } catch (FileNotFoundException e) {
////            e.printStackTrace();
////        } catch (ParseException e) {
////            e.printStackTrace();
////        } catch (IOException e) {
////            e.printStackTrace();
////        }
//    }
//}
